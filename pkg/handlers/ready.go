// Copyright 2017 Aleksandr Polyakov. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	// Alternative of the Bit router with the same Router interface
	// "gitlab.com/dantser/backend/gateway/pkg/router/httprouter"
)

// Ready returns "OK" if service is ready to serve traffic
func (h *Handler) Ready(c *gin.Context) {
	// TODO: possible use cases:
	// load data from a database, a message broker, any external services, etc
	c.String(http.StatusOK, http.StatusText(http.StatusOK))
}
