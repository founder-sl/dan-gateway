package handlers

import (
	"net/http/httputil"
	"net/url"

	"github.com/gin-gonic/gin"
)

// Proxy reverse proxy to module
func (h *Handler) Proxy(c *gin.Context) {
	proxy := httputil.NewSingleHostReverseProxy(&url.URL{
		Scheme: "http",
		Host:   c.Param("module") + "-" + c.Param("version"),
	})
	c.Request.URL.Path = "/"
	if len(c.Param("path")) > 0 {
		c.Request.URL.Path = c.Param("path")
	}
	proxy.ServeHTTP(c.Writer, c.Request)
}
