// Copyright 2017 Aleksandr Polyakov. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package handlers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	// Alternative of the Bit router with the same Router interface
	// "gitlab.com/dantser/backend/gateway/pkg/router/httprouter"
)

// Health returns "OK" if service is alive
func (h *Handler) Health(c *gin.Context) {
	c.String(http.StatusOK, http.StatusText(http.StatusOK))
}
