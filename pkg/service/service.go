// Copyright 2017 Aleksandr Polyakov. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package service

import (
	"regexp"

	"github.com/gin-gonic/gin"

	"github.com/gin-contrib/cors"
	"gitlab.com/dantser/backend/gateway/pkg/config"
	"gitlab.com/dantser/backend/gateway/pkg/handlers"
	"gitlab.com/dantser/backend/gateway/pkg/logger"
	stdlog "gitlab.com/dantser/backend/gateway/pkg/logger/standard"
	"gitlab.com/dantser/backend/gateway/pkg/middlewares"
	"gitlab.com/dantser/backend/gateway/pkg/version"
)

// Setup configures the service
func Setup(cfg *config.Config) (r *gin.Engine, log logger.Logger, err error) {
	// Setup logger
	log = stdlog.New(&logger.Config{
		Level: cfg.LogLevel,
		Time:  true,
		UTC:   true,
	})

	log.Info("Version:", version.RELEASE)
	log.Warnf("%s log level is used", logger.LevelDebug.String())
	log.Infof("Service %s listened on %s:%d", config.SERVICENAME, cfg.LocalHost, cfg.LocalPort)

	// Define handlers
	h := handlers.New(log, cfg)

	// Register new router
	r = gin.Default()

	// CORS middleware
	corsConf := cors.DefaultConfig()
	if cfg.LogLevel == 0 || cfg.CorsOriginMatch == "" {
		corsConf.AllowAllOrigins = true
	} else {
		corsConf.AllowOriginFunc = func(origin string) bool {
			match, err := regexp.MatchString(cfg.CorsOriginMatch, origin)
			if err != nil {
				log.Errorf("Error match allow origin: %s", err.Error())
			}
			return match
		}
	}
	corsConf.AllowMethods = append(corsConf.AllowMethods, "PATCH")
	corsConf.AllowHeaders = append(corsConf.AllowHeaders, "X-REQUESTED-WITH", "DMS-COMPANY-ID", "DMS-FORM-ID", "DMS-POST-ID", "DMS-DEPARTMENT-ID")
	corsConf.AllowCredentials = true
	r.Use(cors.New(corsConf))

	r.Use(h.Base())

	// Configure router
	r.GET("/", h.Root)
	r.GET("/healthz", h.Health)
	r.GET("/readyz", h.Ready)

	// Check auth
	r.Use(middlewares.CheckAuth())

	r.GET("/info", h.Info)
	r.Any(cfg.ApiPrefix+"/:module/:version", h.Proxy)
	r.Any(cfg.ApiPrefix+"/:module/:version/*path", h.Proxy)
	return
}
