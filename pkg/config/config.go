// Copyright 2017 Aleksandr Polyakov. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package config

import (
	"log"

	"github.com/kelseyhightower/envconfig"
	"gitlab.com/dantser/backend/gateway/pkg/logger"
)

const (
	// SERVICENAME contains a service name prefix which used in ENV variables
	SERVICENAME = "API_GATEWAY"
)

var (
	// DOMAIN contains a service domain used in cookie
	DOMAIN string
)

// Config contains ENV variables
type Config struct {
	// Local service host
	LocalHost string `split_words:"true"`
	// Local service port
	LocalPort int `split_words:"true"`
	// Logging level in logger.Level notation
	LogLevel logger.Level `split_words:"true"`
	// Api url prefix
	ApiPrefix string `split_words:"true" default:"/x"`
	// CORS origin match regexp
	CorsOriginMatch string `split_words:"true" default:"^(https?://(?:.+\\.)?dantser\\.net)$"`
	// Account service host name
	AccountHost string `split_words:"true" default:"account-v1"`
	// Orgstructure service host name
	OrgstructureHost string `split_words:"true" default:"orgstructure-v1"`
	// Use secure cookie
	UseSecureCookie bool `split_words:"true" default:"true"`
	// Use http only cookie
	UseHttpOnlyCookie bool `split_words:"true" default:"true"`
	// Domain for cookie
	CookieDomain string `split_words:"true" default:"dantser.net"`
	// Domain for cookie
	CookiePath string `split_words:"true" default:"/"`
	// AID max age (default 60*5)
	CookieAidMaxAge int `split_words:"true" default:"300"`
	// RID max age (default 60*60*24*5)
	CookieRidMaxAge int `split_words:"true" default:"432000"`
}

// Load settles ENV variables into Config structure
func (c *Config) Load(serviceName string) error {
	return envconfig.Process(serviceName, c)
}

// mainConfig main config pointer
var mainConfig *Config

// GetMainConfig get main config
func GetMainConfig() *Config {
	if mainConfig == nil {
		mainConfig = new(Config)
		if err := mainConfig.Load(SERVICENAME); err != nil {
			log.Fatal(err)
		}
	}
	return mainConfig
}
