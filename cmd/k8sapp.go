// Copyright 2017 Aleksandr Polyakov. All rights reserved.
// Use of this source code is governed by a MIT-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"log"

	"gitlab.com/dantser/backend/gateway/pkg/config"
	"gitlab.com/dantser/backend/gateway/pkg/service"
	"gitlab.com/dantser/backend/gateway/pkg/system"
)

func main() {
	// Load ENV configuration (fatal if error)
	cfg := config.GetMainConfig()
	// Configure service and get router
	router, logger, err := service.Setup(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// Listen and serve handlers
	go router.Run(fmt.Sprintf("%s:%d", cfg.LocalHost, cfg.LocalPort))

	// Wait signals
	signals := system.NewSignals()
	if err := signals.Wait(logger, new(system.Handling)); err != nil {
		logger.Fatal(err)
	}
}
